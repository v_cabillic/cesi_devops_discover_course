package com.cesi.devops.course;

import java.util.ArrayList;
import java.util.List;

public class MultipleSum {

    List<Integer> multipleArray;

    public MultipleSum () {
        multipleArray = new ArrayList<>();
    }

    public int sumMultiple(int limit) {
        int sum = 0;
        for (int i = 0; i < limit; i++) {
            if (isNumberMultipleOf3And5(i)) {
                sum += i;
                multipleArray.add(i);
            }
        }
        return sum;
    }

    private boolean isNumberMultipleOf3And5 (int i) {
        return i % 3 == 0 || i % 5 == 0;
    }

    public List<Integer> getMultipleArray() {
        return multipleArray;
    }
}
