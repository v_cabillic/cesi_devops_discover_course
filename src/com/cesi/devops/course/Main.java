package com.cesi.devops.course;

public class Main {

    public static void main(String[] args) {
        MultipleSum multipleSum = new MultipleSum();

        int limit = 1000;

        System.out.println("La somme des multiples de 3 ou 5 inférieurs à " + limit + " est : " + multipleSum.sumMultiple(limit));

    }
}
