package com.cesi.devops.course;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Tests {


    @Test
    void sum() {
        final MultipleSum multipleSum = new MultipleSum();
        assertEquals(233168, multipleSum.sumMultiple(1000));
    }

    @Test
    void sumNotValid() {
        final MultipleSum multipleSum = new MultipleSum();
        assertNotEquals(233168, multipleSum.sumMultiple(56));
    }

    @Test
    void sumTotalRetry() {
        final MultipleSum multipleSum = new MultipleSum();
        for (int i = 0; i < 1000000; i++) {
            assertEquals(233168, multipleSum.sumMultiple(1000));
        }
    }

    @Test
    void isMultipleOf3Or5() {
        final MultipleSum multipleSum = new MultipleSum();
        int multiple = 30;
        multipleSum.sumMultiple(50);
        assertTrue(multipleSum.getMultipleArray().contains(multiple));
    }
}
